# covid-moral-foundations

## Setting up your environment

### Install general dependencies

* Install virtualenv if you do not have it (you just need to do this
  once)

`pip3 --user install virtualenv`

* Create virtual environment (you just need to do this once)

`virtualenv envcovid`

* Activate virtual environment (every time you wish to run this setup)

`source envcovid/bin/activate`

* Install requirements (you just need to do this once)

`pip3 install -r requirements.txt`

### Install and setup DRaiL

* Install gurobi. You can skip this step if running in our lab machines (which you should do!),
  as gurobi is already installed.

* Request an academic license for gurobi and activate it

Go [here](https://www.gurobi.com/downloads/end-user-license-agreement-academic/) and follow instructions to request and activate your academic license. In the lab machines, you can find gurobi under `/opt/gurobi/`, and therefore you can do `/opt/gurobi/bin/grbgetkey [your_key]` to download the license to your home directory. 

Note that you will need a different license file per machine, so make sure to request more licenses and activate each of them in its respective machine. If you use a shared home directory accross machines (standard in our lab), make sure to change the name of the license file in your home directory before activating a new one so that it is not overwritten. 

* Update your environmental vars (if you are running this a lot, add
  this to your `.bashrc`)

```
export GUROBI_HOME=/opt/gurobi
export PATH=${PATH}:${GUROBI_HOME}/bin
export PYTHONPATH=${GUROBI_HOME}/lib/python3.8_utf32:${PYTHONPATH}
export GRB_LICENSE_FILE=$HOME/gurobi.lic
```

**Tip**: If you regularly use more than one lab machine with a shared home directory, you can use this trick in your `.bashrc` (after naming your license files accordingly):

```
case $HOSTNAME in 
  (hostname1.school.edu)
    export GRB_LICENSE_FILE=$HOME/gurobi.lic.hostname1;;
  (hostname2.school.edu)
    export GRB_LICENSE_FILE=$HOME/gurobi.lic.hostname2;;
esac
```

* Clone the DRaiL repository and point your PYTHONPATH variable to the DRaiL directory. 

```
git clone https://gitlab.com/purdueNlp/DRaiL
export PYTHONPATH=${PYTHONPATH}:/path/to/cloned/DRaiL
```

**Whenever you are done running these programs, you can deactivate the
virtualenv by typing:** `deactivate`.

## Downloading data

Download all the needed data from [here](). (**TO-DO**: put data somewhere
other folks can access. This will need some tweaking to not violate
Twitter terms of service. You can skip this step.)


## Running distant supervision baselines

Input parameters make reference to the data downloaded in the previous
step. These are also available in our shared directory (in this case,
replace `data/` with our shared directory path -- or any where you copied the data in our shared directory). All of these targets
assume that you have a GPU available. If you wish to specify a GPU
index (e.g. GPU 1), prepend `CUDA_VISIBLE_DEVICES=1` to your command.
(e.g. ``CUDA_VISIBLE_DEVICES=1 python3 ...``)

### Entity role classifier

Feel free to change the output model path to wherever you want to
save the trained model to.

```
python3 run_role_classifier.py \
  --political_path data/kristen_ds \
  --covid_path data/covid_annotated_tweets.json \
  --no_sentiment \
  --out_model /tmp/role_distant.pt \
```
### Entity sentiment classifier

```
python3 run_entity_sentiment.classifer.py \
  --political_path data/kristen_ds \
  --mpqa_path data/mpqaentsent.csv \
  --covid_path data/covid_annotated_tweets.json \
  --out_model /tmp/sentiment_distant.pt \
```

### Moral foundation classifier

```
python3 run_mf_classifier.py \
  --political_path data/kristen_ds \
  --mftc_path data/MFTC_V4.json \
  --liberty_path data/liberty_opression_data.csv \
  --covid_path data/covid_annotated_tweets.json \
  --out_model /tmp/mf_distant.pt\
  --do_train \
  --do_eval
```

### Morality classifier

```
python3 run_mf_classifier.py \
  --political_path data/kristen_ds \
  --mftc_path data/MFTC_V4.json \
  --liberty_path data/liberty_opression_data.csv \
  --covid_path data/covid_annotated_tweets.json \
  --out_model /tmp/morality_distant.pt \
  --ismoral \
  --do_train \
  --do_eval
```

### Stance classifier

This script will also generate a `correlations.png` file showing
correlations between stance and moral foundations. If you just want to
obtain this file, and not run the train/test, remove the `--do_train`
and `--do_eval` parameters.

```
python3 run_stance_classifier.py \
  --pos_stance_path data/2021_provax_tweets \
  --neg_stance_path data/2021_antivax_tweets \
  --hashtags data_collection/stance_hashtags.json \
  --covid_path data/covid_annotated_tweets.json \
  --out_model /tmp/mf_distant.pt \
  --do_train \
  --do_eval
```

## Running base DRaiL models (supervised)

This target will run the base supervised classifiers, without any dependencies or
constraints. Feel free to change the `savedir` directory to wherever you
want to save your trained models. Once you have trained it, you can
re-run the evaluation by removing the `--do_train` parameter and point to
the same `--savedir`.

```
python3 run_drail.py \
  -r drail_programs/rules/supervised_base.dr \
  -c drail_programs/config/config_base.json \
  --savedir /tmp/supervised_base/ \
  --folds data/covid_annotated_folds.json \
  --tweet2bert data/tweet2bert.json \
  --entity2bert data/entity2bert.json \
  --do_train \
  --do_eval
```

## Running full DRaiL model (supervised)

This target will run the full DRaiL model, with all dependencies and
constraints. Once you have trained it, you can re-run the evaluation by
removing the `--do_train` parameter and point to the same `--savedir`.

```
python3 run_drail.py \
  -r drail_programs/rules/supervised_all.dr \
  -c drail_programs/config/config_all.json \
  --savedir /tmp/supervised_all/ \
  --folds data/covid_annotated_folds.json \
  --tweet2bert data/tweet2bert.json \
  --entity2bert data/entity2bert.json \
  --do_train \
  --do_eval
```

## Running distant supervision model with DRaiL

**TO-DO**

## Running additional viz and analyses

#### Running LDA topic model and generating heatmap matrices


```
python3 run_topic_models.py \
  --unlabeled_data data/2021_US_tweets_text.npy \
  --tweets data/covid_annotated_tweets.json
```

This target will generate the following files:

```
has_theme_0.3_mf_correlations.png
has_theme_0.3_stance_correlations.png
has_theme_mf_correlationss.png
has_theme_stance_correlations.png
has_theme_original_mf_correlations.png
has_theme_original_stance_correlations.png
lda_mf_correlations.png
lda_stance_correlations.png
```

#### See top entities and roles per reason

**TO-DO**
