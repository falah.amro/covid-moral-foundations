from numpy.lib.shape_base import column_stack
import pandas as pd
import numpy as np
import argparse
import sys
import os
import csv

def getEventList(path):
    eventList = os.listdir(path)
    eventpath = [path + '/' + event for event in eventList]
    return eventList, eventpath

def getData(eventList, eventPath, threshold):
    df = pd.read_csv('/scratch3/data/moral_foundations/eventsDate.csv', header=None, index_col=0, squeeze=True)
    eventsDict = df.to_dict()
    #print(eventsDict.keys())
    #print(eventsDateDict['Date'])
    correctEvents = dict()
    for eventpath, event in zip(eventPath, eventList):
        event = event[2:]
        correctEvents[event] = [] # the key event is the data of the event

        # since everything was saved as a string, I had to do a lot of cleaning in order to convert to a list.
        processedDf = pd.read_csv(eventpath) # event name, tweet id, tweet, entities, number of entities matched
        tweetList = processedDf.columns[2][1:]
        tweetList = tweetList[:-1]
        tweetList = tweetList.split(', ')
        #print(tweetList)
        
        entitiesMatched = processedDf.columns[4][1:]
        entitiesMatched = entitiesMatched[:-1]
        entitiesMatched = entitiesMatched.split(', ')
        
        matchedEntities = []
        for elem in entitiesMatched:
            if elem.isnumeric():
                matchedEntities.append(elem)
        matchedEntities = [int(elem) for elem in matchedEntities]
        for tweet, numEntitiesMatched in zip(tweetList, matchedEntities):
            # print(numEntitiesMatched)
            # print(len(set(eventsDict[event])))
            if numEntitiesMatched / len(set(eventsDict[event])) >= threshold:
                correctEvents[event].append([tweet, processedDf.columns[0]]) # stores tweet along with the actual event name

    return correctEvents


        #column_names = ['event_name', 'tweet_ID', 'tweet', 'entities', 'entities_matched']
        
        
def printStats(correctEvents):
    for event in correctEvents.keys():
        print(f"Event {event} had {len(correctEvents[event])} many tweets associated with it.")
        if len(correctEvents[event]) > 0:
            print(f"    EVENT NAME: {correctEvents[event][0][1]}")
            # print(len(correctEvents[event]))
            for i in range(len(correctEvents[event])):
                print(f"        Here are the following tweets related to {event}: {correctEvents[event][i][0]}")
        

def main(args):
    threshold = args.threshold
    path = args.path
    # print(threshold)
    # print(path)
    eventList, eventPath = getEventList(path)
    correctEvents = getData(eventList, eventPath, threshold)
    printStats(correctEvents)
    return



if __name__ == '__main__':

    # to run, pass in the threshold, and the path of the entity files
    # example: python3 entitiesMatched.py --threshold 0.001 --path /scratch3/data/moral_foundations/eventsTweetsData
    parser = argparse.ArgumentParser()
    parser.add_argument('--threshold', required=True, type=float)
    parser.add_argument('--path', required=True, type=str)
    args = parser.parse_args()
    main(args)