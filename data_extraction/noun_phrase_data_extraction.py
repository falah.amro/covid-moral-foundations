import numpy as np
import spacy as sp
import nltk
import pickle
nltk.download('brown')
nltk.download('punkt')
import json
from tqdm import tqdm
from nltk.tokenize import TweetTokenizer
import re
import collections
from textblob import TextBlob


def getNounPhrases(tweetFile):
    tt = TweetTokenizer() # this is a tokenizer for tweets. Tokenizers are used to split text into smaller units
    
    nounPhrases = []
    tweets = json.load(open(tweetFile,))
    pbar = tqdm(total=len(tweets['tweets']))
    for tw in tweets['tweets']:
        text = tw["text"]
        text_lower = text.lower()
        blob = TextBlob(text_lower)
        for noun in blob.noun_phrases:
            nounPhrases.append(noun)
        
        pbar.update(1)
    
    pbar.close()
    return nounPhrases

    # print(f"Tweet Token: {tweet_tokens}")
    # print(f"Full Text: {text}")
    # print(f"lower text: {text_lower}")
def mapToFrequency(nounPhrases):
    frequencies = collections.Counter(nounPhrases)
    # print("In map to frequencies")
    # pbar = tqdm(total=len(set(nounPhrases)))
    # for nounPhrase in set(nounPhrases):
    #     freq = 0
    #     for currNounPhrase in nounPhrases:
    #         if currNounPhrase == nounPhrase:
    #             freq += 1
    #     frequencies[nounPhrase] = freq
    #     pbar.update(1)
    # pbar.close()

    return frequencies

def main():
    tweetFiles = ["/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m1.json", "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m2.json",
    "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m3.json", "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m4.json", 
    "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m5.json", "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m6.json",
    "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m7.json","/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m8.json",
    "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m9.json"]
    
    for i, tweetFile in enumerate(tweetFiles):
        currNounPhrase = getNounPhrases(tweetFile)
        frequencies = mapToFrequency(currNounPhrase)
        fileHandler = "/scratch3/data/CovidTweets/2021_vaccine_tweets/nounPhraseResults/" + "month_" + str(i + 1) + "_nounPhrases"
        filehandler = open(fileHandler, 'wb')
        pickle.dump(frequencies, filehandler)
        filehandler.close()

    print("FINISHED NOUN PHRASE EXTRACTION")
if __name__ == "__main__":
    main()
