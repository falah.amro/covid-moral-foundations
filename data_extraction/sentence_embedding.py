import json
import argparse
import os
from sentence_transformers import SentenceTransformer
import numpy as np

def main(args):
    sentences = []
    for filename in os.listdir(args.tweets_path):
        filepath = os.path.join(args.tweets_path, filename)
        month = json.load(open(filepath))
        print(filepath)
        for tw in month['tweets']:
            sentences.append(tw['text'])

    sentences_arr = np.array(sentences)
    np.save(args.tweet_out, sentences_arr)
    print("Processing sentences: {}".format(sentences_arr.shape))

    model = SentenceTransformer('all-mpnet-base-v2')
    embeddings = model.encode(sentences)
    print("Extracted embedding matrix: {}".format(embeddings.shape))
    np.save(args.embed_out, embeddings)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--tweets_path', required=True, type=str)
    parser.add_argument('--embed_out', required=True, type=str)
    parser.add_argument('--tweet_out', required=True, type=str)
    args = parser.parse_args()
    main(args)
