import numpy as np
import pickle
import json
from tqdm import tqdm
import collections



def getUsernames(tweetFile):
    
    usernames = []
    tweets = json.load(open(tweetFile,))
    pbar = tqdm(total=len(tweets['tweets']))
    for tw in tweets['tweets']:
        # print(tw.keys())
        # print(f"AUTHOR: {tw['author']}")
        # print(f"ENTITIES: {tw['entities']}")
        
        text = tw["author"]["username"]
        text_lower = text.lower()
        usernames.append(text_lower)
        
        pbar.update(1)
    
    pbar.close()
    return usernames


def mapToFrequency(nounPhrases):
    frequencies = collections.Counter(nounPhrases)
    

    return frequencies

def main():
    tweetFiles = ["/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m1.json", "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m2.json",
    "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m3.json", "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m4.json", 
    "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m5.json", "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m6.json",
    "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m7.json","/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m8.json",
    "/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m9.json"]
    
    for i, tweetFile in enumerate(tweetFiles):
        currNounPhrase = getUsernames(tweetFile)
        frequencies = mapToFrequency(currNounPhrase)
        fileHandler = "/scratch3/data/CovidTweets/2021_vaccine_tweets/usernameResults/" + "month_" + str(i + 1) + "_usernames"
        filehandler = open(fileHandler, 'wb')
        pickle.dump(frequencies, filehandler)
        filehandler.close()

    print("FINISHED USERNAME EXTRACTION")
if __name__ == "__main__":
    main()
