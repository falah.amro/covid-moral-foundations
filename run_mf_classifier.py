import argparse
import torch
import json
from torch.utils import data
import os
import numpy as np
from tqdm.auto import tqdm
from collections import Counter
from transformers import AutoTokenizer
from transformers import get_scheduler
from transformers import AdamW
from sklearn.metrics import f1_score, confusion_matrix, classification_report
from sklearn.utils.class_weight import compute_class_weight
from models.bert_classifier import BertClassifier
from sklearn.preprocessing import LabelEncoder
import pickle
import csv

class TextDataset(data.Dataset):

    def __init__(self, input_ids, token_ids, attention_mask, labels, labels_2=None):
        self.input_ids = input_ids
        self.token_ids = token_ids
        self.attention_mask = attention_mask
        self.labels = labels
        self.labels_2 = labels

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, index):
        input_ids_i = self.input_ids[index]
        token_ids_i = self.token_ids[index]
        attention_mask_i = self.attention_mask[index]
        label = self.labels[index]

        if self.labels_2 is not None:
            label_2 = self.labels_2[index]
            return input_ids_i, token_ids_i, attention_mask_i, label, label_2

        return input_ids_i, token_ids_i, attention_mask_i, label

def prepare_mftc_dataset(mftc_path, args):
    tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')
    dataset = json.load(open(mftc_path))

    moral_nonmoral_labels = [];
    moral_foundation_labels = [];
    topic_labels = []

    input_ids = []; token_ids = []; attention_mask = []

    for topic in dataset:
        for tw in topic['Tweets']:
            if 'text' in tw:
                text = tw['text']
                all_anns = []
                for ann in tw['annotations']:
                    all_anns += ann['annotation'].split(',')
                lbl, count = Counter(all_anns).most_common(1)[0]
                if lbl in ['authority', 'subversion']:
                    lbl = 'authority/subversion'
                    moral_foundation_labels.append(lbl)
                    moral_nonmoral_labels.append('moral')
                    topic_labels.append(topic['Corpus'])
                elif lbl in ['care', 'harm']:
                    lbl = 'care/harm'
                    moral_foundation_labels.append(lbl)
                    moral_nonmoral_labels.append('moral')
                    topic_labels.append(topic['Corpus'])
                elif lbl in ['fairness', 'cheating']:
                    lbl = 'fairness/cheating'
                    moral_foundation_labels.append(lbl)
                    moral_nonmoral_labels.append('moral')
                    topic_labels.append(topic['Corpus'])
                elif lbl in ['purity', 'degradation']:
                    lbl = 'purity/degradation'
                    moral_foundation_labels.append(lbl)
                    moral_nonmoral_labels.append('moral')
                    topic_labels.append(topic['Corpus'])
                elif lbl in ['loyalty', 'betrayal']:
                    lbl = 'loyalty/betrayal'
                    moral_foundation_labels.append(lbl)
                    moral_nonmoral_labels.append('moral')
                    topic_labels.append(topic['Corpus'])
                else:
                    moral_nonmoral_labels.append(lbl)
                    moral_foundation_labels.append(lbl)
                    topic_labels.append(topic['Corpus'])

                encoded_inputs = tokenizer(text, padding='max_length', truncation=True, max_length=40)

                input_ids.append(encoded_inputs['input_ids'])
                token_ids.append(encoded_inputs['token_type_ids'])
                attention_mask.append(encoded_inputs['attention_mask'])

    return input_ids, token_ids, attention_mask,\
           moral_nonmoral_labels, moral_foundation_labels, topic_labels

def prepare_political_dataset(political_path, args):
    tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')

    metadata = json.load(open(os.path.join(political_path, "metadata.json")))
    key2text, key2ideo, key2mf_prominent_polarity, key2mf_prominent, key2mf_all, key2topic, key2time = metadata

    moral_nonmoral_labels = [];
    moral_foundation_labels = [];
    topic_labels = []

    mf_set = set()

    encoded_prev = {'input_ids': [], 'token_type_ids': [], 'attention_mask': []}

    data_f = "/homes/pachecog/scratch/DRaiL/examples/mf_role/data/drail_data.pickle"
    with open(data_f, 'rb') as fp:
        [tweet_id2tokens, entity_id2tokens,\
         ideology_id2one_hot,\
         word_dict, vocab_size, word_emb_size, word2idx]  = pickle.load(fp, encoding="latin1")
        idx2word = {v: k for k, v in word2idx.items()}

    data_bert_f = "/homes/pachecog/scratch/DRaiL/examples/mf_role/data/drail_data_bert.pickle"
    with open(data_bert_f, 'rb') as fp:
        [tweet_id2bert, entity_id2bert, ideology_id2one_hot] = pickle.load(fp, encoding="latin1")
    for key in key2text:
        if key in tweet_id2bert:
            # length is 40 always, I checked
            encoded_prev['input_ids'].append(tweet_id2bert[key]['input_ids'])
            encoded_prev['token_type_ids'].append([0] * len(tweet_id2bert[key]['input_ids']))
            encoded_prev['attention_mask'].append(tweet_id2bert[key]['attention_mask'])
            moral_foundation_labels.append(key2mf_prominent[key])
            mf_set.add(key2mf_prominent[key])
            moral_nonmoral_labels.append('moral')
            topic_labels.append(key2topic[key])
        elif key2mf_all[key][0] == 'nm':
            text = key2text[key]
            if key in tweet_id2tokens:
                text = " ".join([idx2word[idx] for idx in tweet_id2tokens[key]])
            encoded_inputs = tokenizer(text, padding='max_length', truncation=True, max_length=40)
            encoded_prev['input_ids'].append(encoded_inputs['input_ids'])
            encoded_prev['token_type_ids'].append(encoded_inputs['token_type_ids'])
            encoded_prev['attention_mask'].append(encoded_inputs['attention_mask'])
            moral_nonmoral_labels.append('non-moral')
            moral_foundation_labels.append('non-moral')
            topic_labels.append(key2topic[key])
        else:
            pass
    #print(np.array(encoded_prev['input_ids']).shape)
    #print(np.array(encoded_prev['token_type_ids']).shape)
    #print(np.array(encoded_prev['attention_mask']).shape)

    input_ids = encoded_prev['input_ids']
    token_ids = encoded_prev['token_type_ids']
    attention_mask = encoded_prev['attention_mask']

    '''
    for key in key2text:
        text = key2text[key]
        if key in tweet_id2tokens:
            text = " ".join([idx2word[idx] for idx in tweet_id2tokens[key]])

        batch_sentences.append(text)

        if key2mf_all[key][0] == 'nm':
            moral_nonmoral_labels.append(0)
        else:
            #print(text)
            moral_nonmoral_labels.append(1)
            moral_foundation_labels.append(key2mf_prominent[key])
            mf_set.add(key2mf_prominent[key])

    print(mf_set)
    #exit()

    encoded_inputs = tokenizer(batch_sentences, padding=True, truncation=True, return_tensors='pt')

    input_ids = encoded_inputs['input_ids'].cuda()
    token_ids = encoded_inputs['token_type_ids'].cuda()
    attention_mask = encoded_inputs['attention_mask'].cuda()
    '''

    return input_ids, token_ids, attention_mask,\
           moral_nonmoral_labels, moral_foundation_labels, topic_labels

def prepare_liberty_oppression_dataset(liberty_path, args):
    tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')
    batch_sentences = []
    moral_nonmoral_labels = []
    moral_foundation_labels = []
    topic_labels = []

    with open(liberty_path) as csv_file:
        reader = csv.reader(csv_file)
        for row in reader:
            text, _, _ = row

            batch_sentences.append(text)
            moral_foundation_labels.append('liberty/oppression')
            moral_nonmoral_labels.append('moral')
            topic_labels.append('unknown')

    encoded_inputs = tokenizer(batch_sentences, padding='max_length', truncation=True, max_length=40)

    input_ids = encoded_inputs['input_ids']
    token_ids = encoded_inputs['token_type_ids']
    attention_mask = encoded_inputs['attention_mask']

    return input_ids, token_ids, attention_mask,\
           moral_nonmoral_labels, moral_foundation_labels, topic_labels


def prepare_covid_dataset(covid_path, args):
    tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')

    dataset = json.load(open(covid_path))

    batch_sentences = [];
    moral_nonmoral_labels = [];
    moral_foundation_labels = []

    mf_set = set()

    for key in dataset:
        if dataset[key]['mf'] is None:
            continue
        if dataset[key]['mf'] == "none" and args.ismoral:
            text = dataset[key]['text']
            batch_sentences.append(text)
            moral_nonmoral_labels.append('non-moral')
        elif dataset[key]['mf'] != "none" and dataset[key]['mf'].strip():
            text = dataset[key]['text']
            batch_sentences.append(text)
            moral_foundation_labels.append(dataset[key]['mf'].strip().replace("sanctity", "purity"))
            mf_set.add(dataset[key]['mf'].strip().replace("sanctity", "purity"))
            moral_nonmoral_labels.append('moral')
        elif dataset[key]['mf'] == "none":
            text = dataset[key]['text']
            batch_sentences.append(text)
            moral_foundation_labels.append('non-moral')

    print(mf_set)

    encoded_inputs = tokenizer(batch_sentences, padding=True, truncation=True, return_tensors='pt')

    input_ids = encoded_inputs['input_ids'].cuda()
    token_ids = encoded_inputs['token_type_ids'].cuda()
    attention_mask = encoded_inputs['attention_mask'].cuda()

    return input_ids, token_ids, attention_mask,\
           moral_nonmoral_labels, moral_foundation_labels


def train(dataset, dev_dataset, model, loss_fn, le, out_model, model_adv, loss_fn_adv):
    train_dataloader = data.DataLoader(dataset, batch_size=32, shuffle=True)
    optimizer = torch.optim.AdamW(model.parameters(), lr=2e-5)
    num_epochs = 3

    PAT = 1
    best_f1 = 0; patience = PAT
    while patience > 0:
        train_loss = 0; train_batches = 0
        y_pred_all = []; y_gold_all = []
        pbar = tqdm(range(len(train_dataloader)))

        model.train()
        for input_ids, token_ids, attention_mask, labels, topic_labels in train_dataloader:
            model.zero_grad()
            logits, probas = model(input_ids, token_ids, attention_mask)
            loss = loss_fn(logits, labels)
            train_loss += loss.item()
            train_batches += 1

            if model_adv is not None:
                model_adv.zero_grad()
                logits, _ = model_adv(input_ids, token_ids, attention_mask)
                loss_adv = loss_fn_adv(logits, topic_labels)
                loss += loss_adv

            _, y_pred = torch.max(probas, 1)
            y_pred_all += list(y_pred.cpu())
            y_gold_all += list(labels.cpu())

            loss.backward()
            optimizer.step()

            pbar.update(1)
        pbar.close()
        train_loss = train_loss / train_batches
        wf1 = f1_score(y_gold_all, y_pred_all, average='weighted')
        mf1 = f1_score(y_gold_all, y_pred_all, average='macro')

        loss_dev, wf1_dev, mf1_dev = predict(dev_dataset, model, loss_fn, le)
        if wf1_dev > best_f1:
            best_f1 = wf1_dev
            patience = PAT
            torch.save(model.state_dict(), out_model)
        else:
            patience -= 1

        print("Train loss: {0}, weighted F1: {1}, macro F1: {2}".format(train_loss, wf1, mf1))
        #print("Dev Perfm: loss: {1}, weighted F1: {2}, macro F1: {3}".format(loss_dev, wf1_dev, mf1_dev))

def predict(dataset, model, loss_fn, le, print_report=False):
    test_dataloader = data.DataLoader(dataset, batch_size=32, shuffle=True)

    model.eval()
    pbar = tqdm(range(len(test_dataloader)))
    test_loss = 0; test_batches = 0
    y_pred_all = []; y_gold_all = []

    for input_ids, token_ids, attention_mask, labels, topic_labels in test_dataloader:
        logits, probas = model(input_ids, token_ids, attention_mask)
        loss = loss_fn(logits, labels)
        test_loss += loss.item()
        test_batches += 1

        _, y_pred = torch.max(probas, 1)
        y_pred_all += list(y_pred.cpu())
        y_gold_all += list(labels.cpu())

        pbar.update(1)

    pbar.close()
    test_loss = test_loss / test_batches
    wf1 = f1_score(y_gold_all, y_pred_all, average='weighted')
    mf1 = f1_score(y_gold_all, y_pred_all, average='macro')

    print("Test loss: {0}, weighted F1: {1}, macro F1: {2}".format(test_loss, wf1, mf1))
    if print_report:
        print(confusion_matrix(y_gold_all, y_pred_all))
        print(classification_report(y_gold_all, y_pred_all, target_names=le.classes_, digits=4))
    return test_loss, wf1, mf1

def compute_weights(Y, output_dim):
    counts = np.bincount(Y)
    num_missing = output_dim - len(counts)
    counts = np.append(counts, np.asarray([0]*num_missing))

    # smoothing
    counts[counts == 0] = 1
    #compute weights
    weights = np.asarray(np.max(counts)*1.0/counts, dtype = "float32")
    return weights


def main(args):
    input_ids = []; token_ids = []; attention_mask = []
    moral_train_labels = []; mf_train_labels = []; topic_train_labels = []

    if args.political_path is not None:
        print("Loading political path")
        _input_ids, _token_ids, _attention_mask,\
            _moral_train_labels, _mf_train_labels, _topic_labels = prepare_political_dataset(args.political_path, args)
        input_ids += _input_ids; token_ids += _token_ids; attention_mask += _attention_mask
        moral_train_labels += _moral_train_labels
        mf_train_labels += _mf_train_labels
        topic_train_labels += _topic_labels

    if args.mftc_path is not None:
        print("Loading mftc path")
        _input_ids, _token_ids, _attention_mask,\
            _moral_train_labels, _mf_train_labels, _topic_labels = prepare_mftc_dataset(args.mftc_path, args)
        input_ids += _input_ids; token_ids += _token_ids; attention_mask += _attention_mask
        moral_train_labels += _moral_train_labels
        mf_train_labels += _mf_train_labels
        topic_train_labels += _topic_labels

    if args.liberty_path is not None:
        print("Loading liberty path")
        _input_ids, _token_ids, _attention_mask,\
            _moral_train_labels, _mf_train_labels, _topic_labels = prepare_liberty_oppression_dataset(args.liberty_path, args)
        input_ids += _input_ids; token_ids += _token_ids; attention_mask += _attention_mask
        moral_train_labels += _moral_train_labels
        mf_train_labels += _mf_train_labels
        topic_train_labels += _topic_labels

    input_ids = torch.LongTensor(input_ids).cuda()
    token_ids = torch.LongTensor(token_ids).cuda()
    attention_mask = torch.LongTensor(attention_mask).cuda()

    input_ids_test, token_ids_test, attention_mask_test,\
        moral_test_labels, mf_test_labels = prepare_covid_dataset(args.covid_path, args)

    le_mf = LabelEncoder()
    le_moral = LabelEncoder()
    le_topic = LabelEncoder()
    le_moral.fit(moral_train_labels)
    le_mf.fit(mf_train_labels)
    topic_labels = le_topic.fit_transform(topic_train_labels)
    topic_labels = torch.LongTensor(topic_labels).cuda()

    le = le_mf
    train_labels = mf_train_labels
    test_labels = mf_test_labels
    if args.ismoral:
        le = le_moral
        train_labels = moral_train_labels
        test_labels = moral_test_labels

    n_classes = len(le.classes_)
    train_labels = le.transform(train_labels)
    train_labels = torch.LongTensor(train_labels).cuda()

    print("Train Stats", Counter(mf_train_labels), len(mf_train_labels))
    print("Test Stats", Counter(mf_test_labels), len(mf_test_labels))
    print("Train Topic Stats", Counter(topic_train_labels), len(topic_train_labels))
    print("Train: ", input_ids.shape, "Dev: ", train_labels.shape)

    train_dataset = TextDataset(input_ids, token_ids, attention_mask, train_labels, topic_labels)
    n_train = int(0.8 * len(train_dataset))
    n_dev = len(train_dataset) - n_train
    train_dataset, dev_dataset = torch.utils.data.random_split(train_dataset, [n_train, n_dev])
    #print(len(train_dataset), len(dev_dataset))

    test_labels = le.transform(test_labels)
    test_labels = torch.LongTensor(test_labels).cuda()

    test_dataset = TextDataset(input_ids_test, token_ids_test, attention_mask_test, test_labels)
    test_dataloader = data.DataLoader(test_dataset, batch_size=32, shuffle=True)

    # get class weights based on training set
    cws = compute_weights(train_labels.cpu().numpy(), n_classes)
    cws = torch.FloatTensor(cws).cuda()

    loss_fn = torch.nn.CrossEntropyLoss(weight=cws)
    model = BertClassifier(n_classes, 'bert-base-uncased')
    model.cuda()

    model_adv = None; loss_fn_adv = None
    if args.adversarial:
        cws_topic = compute_weights(topic_labels.cpu().numpy(), len(le_topic.classes_))
        cws_topic = torch.FloatTensor(cws_topic).cuda()
        loss_fn_adv = torch.nn.CrossEntropyLoss(weight=cws_topic)
        model_adv = BertClassifier(len(le_topic.classes_), 'bert-base-uncased', adversarial=True)
        model_adv.cuda()
        model_adv.bert_model = model.bert_model

    if args.do_train:
        train(train_dataset, dev_dataset, model, loss_fn, le, args.out_model, model_adv, loss_fn_adv)

    if args.do_eval:
        model.load_state_dict(torch.load(args.out_model))
        print("DEV")
        predict(dev_dataset, model, loss_fn, le, print_report=True)
        print("TEST")
        predict(test_dataset, model, loss_fn, le, print_report=True)


if __name__ == "__main__":
    # reproducibility
    torch.manual_seed(42)
    np.random.seed(42)

    parser = argparse.ArgumentParser()
    parser.add_argument('--political_path', type=str, default=None)
    parser.add_argument('--mftc_path', type=str, default=None)
    parser.add_argument('--liberty_path', type=str, default=None)
    parser.add_argument('--covid_path', type=str, required=True)
    parser.add_argument('--ismoral', default=False, action='store_true')
    parser.add_argument('--out_model', type=str, required=True)
    parser.add_argument('--adversarial', default=False, action='store_true')
    parser.add_argument('--do_train', default=False, action='store_true')
    parser.add_argument('--do_eval', default=False, action='store_true')
    args = parser.parse_args()
    main(args)
