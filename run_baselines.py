from sklearn.dummy import DummyClassifier
from sklearn.metrics import *
import numpy as np
import random
import json
import os
import argparse

def main(args):

    predicates = ['has_mf', 'has_morality', 'has_role', 'has_sentiment', 'has_stance', 'has_stance_explicit', 'has_stance_implicit']
    for pred in predicates:
        labels = []; X = []
        with open(os.path.join(args.data, '{}.txt'.format(pred))) as fp:
            for line in fp:
                elems = line.strip().split('\t')
                labels.append(elems[-1])
                X.append(elems[0])
        dummy = DummyClassifier(strategy='uniform')
        dummy.fit(X, labels)
        y_pred = dummy.predict(X)
        print(pred + " | uniform")
        print(classification_report(labels, y_pred, digits=4))
        dummy = DummyClassifier(strategy='most_frequent')
        dummy.fit(X, labels)
        y_pred = dummy.predict(X)
        print(pred + " | most_frequent")
        print(classification_report(labels, y_pred, digits=4))
        print('========================')

if __name__ == "__main__":
    np.random.seed(1534)
    random.seed(1534)
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', type=str, required=True)
    args = parser.parse_args()
    main(args)
