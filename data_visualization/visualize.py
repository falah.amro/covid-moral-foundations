import pickle
import collections
import json
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import nltk
import re
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize


def generateKFrequencies(filename, K):
    with open(filename, 'rb') as file:
        file = pickle.load(file)
        file = cleanData(file)
        sorted_file = sorted(file.items(), key=lambda kv: kv[1], reverse=True)[:K]
        sorted_file = collections.OrderedDict(sorted_file)
        results = []
        for item in sorted_file.keys():
            results.append({item : sorted_file[item]})
        
        return results

def cleanData(file):
    cleanedFile = {}
    stop_words = list(set(stopwords.words('english')))
    pattern = re.compile(r'\s+')
    cleanedFile["covid"] = 0
    for k,v in file.items():
        original_string = k
        testedString = list(re.split('[^a-zA-Z]', k))
        
        testedString = [elem for elem in testedString if elem != '']
        testedString = ' '.join(testedString).strip()
        testedString = testedString.lower()
        word_tokens = word_tokenize(testedString)

        #print(f"original_string: {original_string}")
        #print(f"testedString: {testedString}")
        # if "covid" in word_tokens:
        #     cleanedFile["covid"] += v
        if not all(word in stop_words for word in word_tokens) and "covid" not in word_tokens:
            cleanedFile[k] = v

    return cleanedFile

def visualize(results, name, typeF):
    
    names = []
    frequencies = []
    for freqDict in results:
        names.append(list(freqDict.keys())[0])
        frequencies.append(list(freqDict.values())[0])
    plt.figure(figsize=(55, 10))
    plt.xticks(rotation=35)
    plt.bar(names, frequencies, align='center')
    fileHandler = typeF + "_" + name + '.png'
    title = "Names V.S Frequencies For " + name
    plt.suptitle(title)
    plt.savefig(fileHandler, dpi=400)

def main():
    nounPhraseFileList = ['/scratch3/data/CovidTweets/2021_vaccine_tweets/nounPhraseResults/month_1_nounPhrases',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/nounPhraseResults/month_2_nounPhrases',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/nounPhraseResults/month_3_nounPhrases',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/nounPhraseResults/month_4_nounPhrases',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/nounPhraseResults/month_5_nounPhrases',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/nounPhraseResults/month_6_nounPhrases',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/nounPhraseResults/month_7_nounPhrases',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/nounPhraseResults/month_8_nounPhrases',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/nounPhraseResults/month_9_nounPhrases']

    for i, nounPhraseFile in enumerate(nounPhraseFileList):
        results = generateKFrequencies(nounPhraseFile, 50)
        name = "Month " + str(i + 1)
        typeF = "noun_phrase"
        visualize(results, name, typeF)
        
        
    namedEntityResultsList = ['/scratch3/data/CovidTweets/2021_vaccine_tweets/namedEntityResults/month_1_namedEntity',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/namedEntityResults/month_2_namedEntity',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/namedEntityResults/month_3_namedEntity',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/namedEntityResults/month_4_namedEntity',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/namedEntityResults/month_5_namedEntity',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/namedEntityResults/month_6_namedEntity',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/namedEntityResults/month_7_namedEntity',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/namedEntityResults/month_8_namedEntity',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/namedEntityResults/month_9_namedEntity']

    for i, namedEntityFile in enumerate(namedEntityResultsList):
        results = generateKFrequencies(namedEntityFile, 50)
        name = "Month " + str(i + 1)
        typeF = "named_entity"
        visualize(results, name, typeF)
        
    usernameResultsList = ['/scratch3/data/CovidTweets/2021_vaccine_tweets/usernameResults/month_1_usernames',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/usernameResults/month_2_usernames',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/usernameResults/month_3_usernames',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/usernameResults/month_4_usernames',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/usernameResults/month_5_usernames',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/usernameResults/month_6_usernames',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/usernameResults/month_7_usernames',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/usernameResults/month_8_usernames',
    '/scratch3/data/CovidTweets/2021_vaccine_tweets/usernameResults/month_9_usernames']

    for i, usernameFile in enumerate(usernameResultsList):
        results = generateKFrequencies(usernameFile, 50)
        name = "Month " + str(i + 1)
        typeF = "username"
        visualize(results, name, typeF)
        
if __name__ == "__main__":
    main()

