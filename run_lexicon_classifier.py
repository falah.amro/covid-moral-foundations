import json
import argparse
import numpy as np
from tqdm import tqdm
from nltk.tokenize import TweetTokenizer
import re
import os
from collections import Counter
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import f1_score, confusion_matrix, classification_report

def prepare_covid_dataset(args):
    dataset = json.load(open(args.covid_path))
    batch_sentences = [];
    moral_nonmoral_labels = [];
    moral_foundation_labels = []

    mf_set = set()

    for key in dataset:
        if dataset[key]['mf'] is None:
            continue

        if dataset[key]['mf'] == "none":
            text = dataset[key]['text']
            batch_sentences.append(text)
            moral_nonmoral_labels.append('non-moral')
            moral_foundation_labels.append('non-moral')
        else:
            text = dataset[key]['text']
            batch_sentences.append(text)
            moral_foundation_labels.append(dataset[key]['mf'].strip().replace("sanctity", "purity"))
            mf_set.add(dataset[key]['mf'].strip().replace("sanctity", "purity"))
            moral_nonmoral_labels.append('moral')
    print(mf_set)
    return batch_sentences, moral_nonmoral_labels, moral_foundation_labels

def translate(label):
    label = label.lower()
    if label.startswith('care') or label.startswith('harm'):
        return 'care/harm'
    elif label.startswith('fairness') or label.startswith('cheating'):
        return 'fairness/cheating'
    elif label.startswith('lotalty') or label.startswith('betrayal') or label.startswith('ingroup'):
        return 'loyalty/betrayal'
    elif label.startswith('authority') or label.startswith('subversion'):
        return 'authority/subversion'
    elif label.startswith('purity') or label.startswith('sanctity') or label.startswith('degradation'):
        return 'purity/degradation'
    elif label.startswith('moral'):
        return 'moral'
    return 'non-moral'

def get_mfd_keywords(filename, v2=False):
    id2mf = {}; key2mf = {}; keystar2mf = {}
    step = 0
    with open(filename) as fp:
        for line in fp:
            if line.startswith('%'):
                step += 1
                continue
            elems = line.strip().split()
            if len(elems) >= 2:
                if v2 and step > 1:
                    words = tuple(elems[:-1])
                    labels = [elems[-1]]
                else:
                    words = elems[0]
                    labels = elems[1:]
                if step == 1:
                    id2mf[words] = labels[0]
                else:
                    if isinstance(words, str) and words.endswith('*'):
                        key2mf[words[:-1]] = [translate(id2mf[x]) for x in labels]
                    else:
                        key2mf[words] = [translate(id2mf[x]) for x in labels]
                    keystar2mf[words] = [translate(id2mf[x]) for x in labels]
    return id2mf, keystar2mf, key2mf

def get_liberty_keywords(filename):
    key2mf = {}; keystar2mf = {}
    with open(filename) as fp:
        for i, line in enumerate(fp):
            if i == 0:
                continue
            words = tuple(line.strip().split())
            key2mf[words] = ['liberty/oppression']
            keystar2mf[words] = ['liberty/oppression']
    return keystar2mf, key2mf

def predict_lexicon(tweets, args):
    if args.mfd is not None:
        id2mf, keystar2mf, key2mf = get_mfd_keywords(args.mfd)
    if args.mfd2 is not None:
        id2mf2, keystar2mf2, key2mf2 = get_mfd_keywords(args.mfd2, v2=True)
    if args.liberty is not None:
        keystar2mf3, key2mf3 = get_liberty_keywords(args.liberty)

    y_pred = []; count_undecided = 0
    tt = TweetTokenizer()
    for tweet in tweets:
        labels = []
        tweet = tweet.lower()
        tokens = tt.tokenize(tweet)
        for i, tok in enumerate(tokens):
            if args.mfd:
                for moral_key in keystar2mf:
                    if (moral_key.endswith('*') and tok.startswith(moral_key[:-1])) or tok == moral_key:
                        labels += keystar2mf[moral_key]
            if args.mfd2:
                for moral_keys in keystar2mf2:
                    if (len(moral_keys) == 1 and tok == moral_keys[0]) or \
                       (len(moral_keys) == 2 and tok == moral_keys[0] and (i+1 < len(tokens)) and tokens[i+1] == moral_keys[1]) or \
                       (len(moral_keys) == 3 and tok == moral_keys[0] and (i+2 < len(tokens)) and tokens[i+1] == moral_keys[1] and tokens[i+2] == moral_keys[2]) or \
                       (len(moral_keys) == 4 and tok == moral_keys[0] and (i+3 < len(tokens)) and tokens[i+1] == moral_keys[1] and tokens[i+2] == moral_keys[2] and tokens[i+3] == moral_keys[3]):
                        labels += keystar2mf2[moral_keys]
            if args.liberty:
                for moral_keys in keystar2mf3:
                    if (len(moral_keys) == 1 and tok == moral_keys[0]) or \
                       (len(moral_keys) == 2 and tok == moral_keys[0] and (i+1 < len(tokens)) and tokens[i+1] == moral_keys[1]) or \
                       (len(moral_keys) == 3 and tok == moral_keys[0] and (i+2 < len(tokens)) and tokens[i+1] == moral_keys[1] and tokens[i+2] == moral_keys[2]) or \
                       (len(moral_keys) == 4 and tok == moral_keys[0] and (i+3 < len(tokens)) and tokens[i+1] == moral_keys[1] and tokens[i+2] == moral_keys[2] and tokens[i+3] == moral_keys[3]):
                        labels += keystar2mf3[moral_keys]
        if len(labels) == 0:
            y_pred.append('non-moral')
            count_undecided += 1
        elif args.ismoral:
            y_pred.append('moral')
        else:
            counter = Counter(labels).most_common(2)
            if counter[0][0] == 'moral' and ((len(counter) > 2 and counter[1][1] > counter[2][1]) or (len(counter) == 2)):
                y_pred.append(counter[1][0])
            elif counter[0][0] == 'moral' and len(counter) > 2:
                maxim = counter[1][1]
                elems = []
                for elem, c in counter[:1]:
                    if c == maxim:
                        elems.append(e)
                elems.sort()
                y_pred.append(elems[0])
            elif len(counter) > 1 and counter[0][1] == counter[1][0]:
                maxim = counter[0][1]
                elems = []
                for elem, c in counter:
                    if c == maxim:
                        elems.append(e)
                elems.sort()
                y_pred.append(elems[0])
            else:
                y_pred.append(counter[0][0])

    return y_pred

def main(args):

    test_tweets, moral_test_labels, mf_test_labels =\
            prepare_covid_dataset(args)
    print(Counter(mf_test_labels), len(mf_test_labels))
    le_mf = LabelEncoder()
    le_moral = LabelEncoder()
    moral_test_labels = le_moral.fit_transform(moral_test_labels)
    mf_test_labels = le_mf.fit_transform(mf_test_labels)

    test_labels = mf_test_labels
    if args.ismoral:
        test_labels = moral_test_labels

    pred_labels = predict_lexicon(test_tweets, args)
    if args.ismoral:
        pred_labels = le_moral.transform(pred_labels)
        le = le_moral
    else:
        le_mf.classes_ = np.append(le_mf.classes_, 'moral')
        pred_labels = le_mf.transform(pred_labels)
        le = le_mf

    print(confusion_matrix(test_labels, pred_labels))
    print(classification_report(test_labels, pred_labels, target_names=le.classes_, digits=4, labels=list(set(test_labels))))

if __name__ == "__main__":
    # reproducibility
    np.random.seed(42)

    parser = argparse.ArgumentParser()
    parser.add_argument('--mfd', type=str, default=None)
    parser.add_argument('--mfd2', type=str, default=None)
    parser.add_argument('--liberty', type=str, default=None)
    parser.add_argument('--covid_path', type=str, required=True)
    parser.add_argument('--ismoral', default=False, action='store_true')
    args = parser.parse_args()
    main(args)
