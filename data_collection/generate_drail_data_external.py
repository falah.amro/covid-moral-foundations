import json
import argparse
from transformers import AutoTokenizer
from tqdm import tqdm
import os
import wikipedia
from collections import Counter
import networkx as nx
from sentence_transformers import SentenceTransformer
from scipy.spatial import distance
import numpy as np
import spacy
from datetime import datetime, timedelta


def entity_group_lexform(entity2lexform, entity2tweet, tweet2date, dataset, covid_data):
    all_entities = []; lexform2ids ={}; tweet_groups = {}
    pbar = tqdm(total=len(entity2lexform), desc='grouping entities based on lexform')
    for ent in entity2lexform:
        tw = entity2tweet[ent]
        date = tweet2date[tw]
        lexform = entity2lexform[ent].lower()
        all_entities.append(lexform+date)
        if lexform+date not in lexform2ids:
            lexform2ids[lexform+date] = []
        lexform2ids[lexform+date].append(ent)
        pbar.update(1)
    pbar.close()
    for group, lexform in enumerate(Counter(all_entities)):
        for idx in lexform2ids[lexform]:
            dataset['entity_group_lexform'].add((str(idx), str(group)))
            if entity2tweet[idx] not in tweet_groups:
                tweet_groups[entity2tweet[idx]] = set()
            tweet_groups[entity2tweet[idx]].add(str(group))

    find_connected_components(tweet_groups, dataset, covid_data, 'in_instance_lexform')

def entity_group_wikipedia(entity2lexform, entity2tweet, dataset, covid_data):
    all_entities = []; wikipedia2ids = {}; tweet_groups = {}
    pbar = tqdm(total=len(entity2lexform), desc='grouping entities based on wikipedia')
    for ent in entity2lexform:
        lexform = entity2lexform[ent]
        ret = wikipedia.search(lexform)
        if len(ret) > 0:
            ret = ret[0]
        else:
            ret = lexform
        if ret not in wikipedia2ids:
            wikipedia2ids[ret] = []
        wikipedia2ids[ret].append(ent)
        all_entities.append(ret)
        pbar.update(1)
    pbar.close()
    for group, lexform in enumerate(Counter(all_entities)):
        for idx in wikipedia2ids[lexform]:
            dataset['entity_group_wikipedia'].add((str(idx), str(group)))
            if entity2tweet[idx] not in tweet_groups:
                tweet_groups[entity2tweet[idx]] = set()
            tweet_groups[entity2tweet[idx]].add(str(group))
    find_connected_components(tweet_groups, dataset, covid_data, 'in_instance_wikipedia')

def find_connected_components(tweet_groups, dataset, covid_data, filename):
    g = nx.Graph()
    for tw in tweet_groups:
        g.add_node(tw)
        for tw_2 in tweet_groups:
            if tw != tw_2 and len(tweet_groups[tw] & tweet_groups[tw_2]) > 0:
                if not g.has_node(tw_2):
                    g.add_node(tw_2)
                g.add_edge(tw, tw_2)
    seen_tweets = set()
    n_component = 0
    component_size = []
    for i, connected_comp in enumerate(nx.connected_components(g)):
        for tw in connected_comp:
            dataset[filename].add((str(tw), str(n_component)))
            seen_tweets.add(tw)
        component_size.append(len(connected_comp))
        n_component += 1
    for tw in covid_data:
        if tw not in seen_tweets:
            dataset[filename].add((str(tw), str(n_component)))
            n_component += 1
    print(Counter(component_size).most_common(10))

def compute_theme_embeddings(model, themes):
    theme_embeddings = {}
    for theme in themes:
        embeddings = []
        for phrase in themes[theme]:
            embeddings.append(model.encode([phrase.lower()])[0])
        theme_embeddings[theme] = embeddings
    return theme_embeddings

def get_tweet_theme(model, tweet, themes, theme_embeddings):
    tweet_embed = model.encode([tweet])[0]
    min_dis = []; curr_themes = []
    for theme, val in themes.items():
        dis = []
        for i in range (0, len(val)): #phrases in each theme
            dis.append(distance.cdist(tweet_embed.reshape(1 , 768), theme_embeddings[theme][i].reshape(1 , 768), 'cosine'))
        min_dis.append(np.array(dis).min(axis=0).item())
        curr_themes.append(theme)
    min_index = np.array(min_dis).argmin(axis=0).item()
    min_dist = min_dis[min_index]
    theme = curr_themes[min_index]
    return theme, min_dist

def main(args):
    dataset = {
        'is_tweet': set(),
        'has_entity': set(),
        'has_theme_0.3': set(),
        'has_theme_0.5': set(),
        'has_theme': set(),
        'entity_group_lexform': set(),
        'entity_group_wikipedia': set(),
        'in_instance_lexform': set(),
    }

    tokenizer = AutoTokenizer.from_pretrained('bert-base-uncased')
    sbert_model = SentenceTransformer('all-mpnet-base-v2')

    themes = json.load(open(args.theme_path))
    theme_embeddings = compute_theme_embeddings(sbert_model, themes)


    entity_idx = 0
    tweet2bert = {}; entity2bert = {}; entity2lexform = {}; entity2tweet = {}; tweet2date = {}
    nlp = spacy.load("en_core_web_sm")

    tweet_idx = 0; entity_idx = 0
    for filename in os.listdir(args.covid_path):
        print(filename)
        filepath = os.path.join(args.covid_path, filename)
        covid_data = json.load(open(filepath))
        print(filepath, len(covid_data['tweets']))
        pbar = tqdm(total=len(covid_data['tweets']), desc='processing {}'.format(filename))
        for tw in covid_data['tweets']:
            tw_id = tw['id']

            # get the date
            value = tw['created_at']
            value = value.replace("T", " ")
            value = value[2:len(value)-5]
            date_time_obj = datetime.strptime(value, '%y-%m-%d %H:%M:%S')
            date_time_obj = date_time_obj.replace(hour=0, minute=0, second=0)
            date_time_str = str(date_time_obj)
            tweet2date[tw_id] = date_time_str

            dataset['is_tweet'].add((tw_id,))
            tweet_bert = tokenizer(tw['text'], padding='max_length', truncation=True, max_length=100)
            tweet2bert[tw_id] = dict(tweet_bert)

            theme, dist = get_tweet_theme(sbert_model, tw['text'].lower(),
                                           themes, theme_embeddings)
            if dist < 0.3:
                dataset['has_theme_0.3'].add((str(tw_id), theme))
            elif dist < 0.5:
                dataset['has_theme_0.5'].add((str(tw_id), theme))
            dataset['has_theme'].add((str(tw_id), theme))

            doc = nlp(tw['text'])
            for ent in doc.noun_chunks:
                dataset['has_entity'].add((tw_id, str(entity_idx)))
                tweet_entity_bert = tokenizer(tw['text'], ent.text, padding='max_length', truncation=True, max_length=100)
                entity2bert[entity_idx] = dict(tweet_entity_bert)

                entity2lexform[entity_idx] = ent.text
                entity2tweet[entity_idx] = tw_id

                entity_idx += 1
            pbar.update(1)
        pbar.close()

    entity_group_lexform(entity2lexform, entity2tweet, tweet2date, dataset, covid_data)
    #entity_group_wikipedia(entity2lexform, entity2tweet, dataset, covid_data)

    with open(os.path.join(args.out_dir_bert, 'tweet2bert_unlabeled.json'), 'w') as fp:
        json.dump(tweet2bert, fp)
    with open(os.path.join(args.out_dir_bert, 'entity2bert_unlabeled.json'), 'w') as fp:
        json.dump(entity2bert, fp)
    with open(os.path.join(args.out_dir_bert, 'entity2lexform_unlabeled.json'), 'w') as fp:
        json.dump(entity2lexform, fp)


    for predicate in dataset:
        with open(os.path.join(args.out_dir_drail, predicate + '.txt'), 'w') as fp:
            for tup in dataset[predicate]:
                fp.write('\t'.join(tup))
                fp.write('\n')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--covid_path', type=str, required=True)
    parser.add_argument('--theme_path', type=str, required=True)
    parser.add_argument('--out_dir_bert', type=str, required=True)
    parser.add_argument('--out_dir_drail', type=str, required=True)
    args = parser.parse_args()
    main(args)
