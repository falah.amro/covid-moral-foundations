import tweepy
import json
import argparse


def main(args):
    config = json.load(open(args.config))
    auth = tweepy.OAuthHandler(config['consumer_key'], config['consumer_secret'])
    auth.set_access_token(config['access_token'], config['access_token_secret'])
    api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    query = '(covid AND vaccine) lang:en -is:retweet'
    output = {'tweets': []}

    n_written = 0

    for tweet in tweepy.Cursor(api.search, q=query, lang='en', tweet_mode='extended', retry_count=10, retry_delay=5, retry_errors=set([503])).items():
        tweet_json = json.dumps(tweet._json)
        output['tweets'].append(tweet_json)

        n_written += 1

        if n_written % 1000 == 0:
            print("{} tweets written".format(n_written))
        if n_written % 100 == 0:
            with open("/scratch2/data/CovidTweets/tweets.json", "w", encoding="utf-8") as fp:
                json.dump(output, fp)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', required=True, type=str)
    args = parser.parse_args()
    main(args)
