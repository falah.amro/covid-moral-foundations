from sentence_transformers import SentenceTransformer
from scipy.spatial import distance
import numpy as np
import json
import argparse
from tqdm import tqdm
import os

def compute_theme_embeddings(model, themes):
    theme_embeddings = {}
    for theme in themes:
        embeddings = []
        for phrase in themes[theme]:
            embeddings.append(model.encode([phrase.lower()])[0])
        theme_embeddings[theme] = embeddings
    return theme_embeddings

def get_tweet_theme(model, tweet, themes, theme_embeddings):
    tweet_embed = model.encode([tweet])[0]
    min_dis = []; curr_themes = []
    for theme, val in themes.items():
        dis = []
        for i in range (0, len(val)): #phrases in each theme
            dis.append(distance.cdist(tweet_embed.reshape(1 , 768), theme_embeddings[theme][i].reshape(1 , 768), 'cosine'))
        min_dis.append(np.array(dis).min(axis=0).item())
        curr_themes.append(theme)
    min_index = np.array(min_dis).argmin(axis=0).item()
    min_dist = min_dis[min_index]
    theme = curr_themes[min_index]
    return theme, min_dist

def main(args):
    dataset = {
        args.theme_name: set(),
    }

    sbert_model = SentenceTransformer('all-mpnet-base-v2')

    themes = json.load(open(args.theme_path))
    theme_embeddings = compute_theme_embeddings(sbert_model, themes)

    with open(args.covid_path) as fp:
        covid_data = json.load(fp)
        pbar = tqdm(total=len(covid_data))

        for tw_id in covid_data:

            theme, dist = get_tweet_theme(sbert_model, covid_data[tw_id]['text'].lower(),
                                           themes, theme_embeddings)
            dataset[args.theme_name].add((str(tw_id), theme))
            pbar.update(1)
        pbar.close()

    for predicate in dataset:
        with open(os.path.join(args.out_dir_drail, predicate + '.txt'), 'w') as fp:
            for tup in dataset[predicate]:
                fp.write('\t'.join(tup))
                fp.write('\n')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--covid_path', type=str, required=True)
    parser.add_argument('--theme_path', type=str, required=True)
    parser.add_argument('--theme_name', type=str, required=True)
    parser.add_argument('--out_dir_drail', type=str, required=True)
    args = parser.parse_args()
    main(args)
