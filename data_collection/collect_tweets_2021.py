import json
import argparse
import datetime
from twarc.client2 import Twarc2
from twarc.expansions import ensure_flattened
from tqdm import tqdm
import os

def main(args):
    # Your bearer token here
    config = json.load(open(args.config))
    t = Twarc2(consumer_key=config['consumer_key'], consumer_secret=config['consumer_secret'])

    #query = '(covid AND vaccine) lang:en -is:retweet'
    query = '(covid vaccine) lang:en -is:retweet'
    for month in range(args.start_month, 10):
        prev_week = 1

        if os.path.isfile("/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m{}.json".format(month)):
            output = json.load(open("/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m{}.json".format(month), encoding='utf-8'))
        else:
            output = {'tweets': []}

        print("Loaded initial file with {0} tweets for month {1}".format(len(output['tweets']), month))

        for week in range(args.period_day, 31, 10):
            #print(week)
            start_time = datetime.datetime(2021, month, prev_week, 0, 0, 0, 0, datetime.timezone.utc)
            if week == 30 and month == 2:
                end_time = datetime.datetime(2021, month, 28, 11, 59, 0, 0, datetime.timezone.utc)
                week = 28
            elif week == 30 and month in [1, 3, 5, 7, 8]:
                end_time = datetime.datetime(2021, month, 31, 11, 59, 0, 0, datetime.timezone.utc)
                week = 31
            elif week == 30 and month == 9:
                end_time = datetime.datetime(2021, month, 26, 5, 0, 0, 0, datetime.timezone.utc)
            else:
                end_time = datetime.datetime(2021, month, week, 11, 59, 0, 0, datetime.timezone.utc)

            print(start_time, "----", end_time)
            n_tweets = 0

            search_results = t.search_all(query=query, max_results=100, start_time=start_time, end_time=end_time)

            pbar = tqdm(total=100000)
            for page in search_results:
                for tweet in ensure_flattened(page):
                    output['tweets'].append(tweet)
                    n_tweets += 1
                    pbar.update(1)
                # Up to 100,000 in a 10-day period
                if n_tweets >= 100000:
                    break

            with open('/scratch3/data/CovidTweets/2021_vaccine_tweets/tweets_2021_m{0}.json'.format(month), "w", encoding='utf-8') as fp:
                json.dump(output, fp)

            print("Wrote {0} tweets for period in month {1} from day {2} to day {3}".format(n_tweets, month, prev_week, week))
            pbar.close()
            prev_week = week

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', required=True, type=str)
    parser.add_argument('--start_month', type=int, default=1)
    parser.add_argument('--period_day', type=int, default=10)
    args = parser.parse_args()
    main(args)
