import json
import logging.config
import argparse
import numpy as np
from sklearn.metrics import *
import torch
import random
from pathlib import Path
import os
from drail import database
from drail.learn.local_learner import LocalLearner

def main(args):
    folds = json.load(open(args.folds))

    optimizer = "AdamW"
    if args.gpu_index:
        torch.cuda.set_device(args.gpu_index)

    learner = LocalLearner()
    learner.compile_rules(args.rules)
    db = learner.create_dataset(args.dir)

    avg_metrics = {}
    for i in folds:
        if not args.single_fold:
            test_tweets = folds[i]
            train_tweets = []
            for k in folds:
                if k != i:
                    train_tweets += folds[k]
            dev_split = int(len(train_tweets) * 0.8)
            dev_tweets = train_tweets[dev_split:]
            train_tweets = train_tweets[:dev_split]
        elif not args.do_train:
            test_tweets = []
            for k in folds:
                test_tweets += folds[k]
            # Adding this here so code doesn't break, but it is meaningless
            train_tweets = test_tweets[:1]
            dev_tweets = test_tweets[:1]
        else:
            print('If you are training you need to use K fold cross-val')
            exit()

        torch.cuda.empty_cache()
        curr_savedir = os.path.join(args.savedir, "f{}".format(i))
        Path(curr_savedir).mkdir(parents=True, exist_ok=True)

        if args.debug:
            train_tweets = train_tweets[:10]
            dev_tweets = dev_tweets[:10]
            test_tweets = test_tweets[:10]

        logger.info("train {}, dev {}, test {}".format(len(train_tweets), len(dev_tweets), len(test_tweets)))

        db.add_filters(filters=[
            ("IsTweet", "isDummy", "tweetId_1", train_tweets[0:1]),
            ("IsTweet", "isTrain", "tweetId_1", train_tweets),
            ("IsTweet", "isDev", "tweetId_1", dev_tweets),
            ("IsTweet", "isTest", "tweetId_1", test_tweets)
        ])

        learner.build_feature_extractors(db,
                tweets_f=args.tweet2bert,
                entities_f=args.entity2bert,
                femodule_path=args.fe_path,
                filters=[("IsTweet", "isDummy", 1)])

        learner.set_savedir(curr_savedir)
        learner.build_models(db, args.config, netmodules_path=args.ne_path)

        if args.do_train:
            learner.train(db,
                          train_filters=[("IsTweet", "isTrain", 1)],
                          dev_filters=[("IsTweet", "isDev", 1)],
                          test_filters=[("IsTweet", "isTest", 1)],
                          optimizer=optimizer)

        if args.do_eval:
            learner.init_models()
            learner.extract_instances(db,
                    extract_test=True, test_filters=[("IsTweet", "isTest", 1)])
            res, heads = learner.predict(db, fold_filters=[("IsTweet", "isTest", 1)], fold='test', get_predicates=True)
            for pred in res.metrics:
                if pred in set(['HasRole', 'HasMoralFoundation', 'HasSentiment', 'HasStance', 'HasMorality']):
                    y_gold = res.metrics[pred]['gold_data']
                    y_pred = res.metrics[pred]['pred_data']
                    if pred not in avg_metrics:
                        avg_metrics[pred] = {}
                        avg_metrics[pred]['gold'] = y_gold
                        avg_metrics[pred]['pred'] = y_pred
                    else:
                        avg_metrics[pred]['gold'].extend(y_gold)
                        avg_metrics[pred]['pred'].extend(y_pred)

                    #logger.info('\n'+ pred + ':\n' + classification_report(y_gold, y_pred, digits=4))
            learner.reset_metrics()

        if args.single_fold:
            break

    if args.do_eval:
        for pred in avg_metrics:
            if pred == 'HasStance':
                logger.info(pred + ':\n' + classification_report(avg_metrics[pred]['gold'], avg_metrics[pred]['pred'], digits=4))
            else:
                logger.info(pred + ':\n' + classification_report(avg_metrics[pred]['gold'], avg_metrics[pred]['pred'], digits=4))


if __name__ == "__main__":
    # seed and cuda
    torch.manual_seed(1534)
    np.random.seed(1534)
    random.seed(1534)

    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--gpu', help='gpu index', dest='gpu_index', type=int, default=0)
    parser.add_argument('-d', '--dir', help='directory', dest='dir', type=str, default='drail_programs/data')
    parser.add_argument('-r', '--rule', help='rule file', dest='rules', type=str, required=True)
    parser.add_argument('-c', '--config', help='config file', dest='config', type=str, required=True)
    parser.add_argument('--savedir', help='save directory', dest='savedir', type=str, required=True)
    parser.add_argument('--folds', help='folds file', dest='folds', type=str, required=True)
    parser.add_argument('--debug', help='debug mode', dest='debug', default=False, action='store_true')
    parser.add_argument('--do_eval', default=False, action='store_true')
    parser.add_argument('--do_train', default=False, action='store_true')
    parser.add_argument('--tweet2bert', type=str, required=True)
    parser.add_argument('--entity2bert', type=str, required=True)
    parser.add_argument('--fe_path', type=str, default='drail_programs/feats/')
    parser.add_argument('--ne_path', type=str, default='drail_programs/neuro/')
    parser.add_argument('--log', type=str, default='logging_conf.json')
    parser.add_argument('--single_fold', default=False, action='store_true')
    parser.add_argument('--out_dir', type=str)
    args = parser.parse_args()

    logger = logging.getLogger()
    logging.config.dictConfig(json.load(open(args.log)))
    main(args)


